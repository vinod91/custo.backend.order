﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.Runtime;
using Custo.Backend.Order.Entities.DB;
using Newtonsoft.Json;
using System.Linq;

namespace Custo.Backend.Order.DataAccess
{
    public class OrderRepository: IOrderRepository
    {
        private readonly AmazonDynamoDBClient _client;

        public OrderRepository()
        {
            var credentials = new BasicAWSCredentials("AKIAJFK74RHXJMVFEKVQ", "F9hVdrtds5fByi72NgjGKVYbNHT7SeqqMML8dGt7");
            _client = new AmazonDynamoDBClient(credentials, RegionEndpoint.USEast1);
        }
        public async Task<Dictionary<string,HashSet<int>>[]> FetchChefDetails(int restaurantId)
        {
            Table table = Table.LoadTable(_client, "RestaurantChef");
            GetItemOperationConfig getItemOperationConfig = new GetItemOperationConfig();
            var chefDynamoDB = await table.GetItemAsync(restaurantId, getItemOperationConfig);
            var chefDetails = JsonConvert.DeserializeObject<ChefDetails>(chefDynamoDB.ToJson()).ChefId;
            return chefDetails;
        }
    }
}
