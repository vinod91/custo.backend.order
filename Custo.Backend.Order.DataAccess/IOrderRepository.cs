﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Custo.Backend.Order.Entities.DB;

namespace Custo.Backend.Order.DataAccess
{
    public interface IOrderRepository
    {
        Task<Dictionary<string, HashSet<int>>[]> FetchChefDetails(int restaurantId);
    }
}
