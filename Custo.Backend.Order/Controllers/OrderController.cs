﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Custo.Backend.Order.Business;
using Custo.Backend.Order.Entities.APIRequest;
using Custo.Backend.Order.Notification;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;

namespace Custo.Backend.Order.Controllers
{
    //[Authorize(Policy = "Customers")]
    [Route("api/[controller]")]
    public class OrderController : Controller
    {
        private readonly IHubContext<NotificationHub> _hubContext;
        private IConnectionManager _connectionManager;
        public OrderController(IHubContext<NotificationHub> hubContext, IConnectionManager connectionManager)
        {
            _hubContext = hubContext;
            _connectionManager = connectionManager;
        }

        [HttpPost]
        [Route("ReceiveOrder")]
        public IActionResult ReceiveOrder([FromBody] OrderRequest request)
        {
            //Capture the order request in sql
            var connectionIds = _connectionManager.GetConnections("handlerId");
            foreach (var connectionId in connectionIds)
                _hubContext.Clients.Client(connectionId).InvokeAsync("OnOrderReceived", JsonConvert.SerializeObject(request));
            return Ok();
        }

        [HttpPost]
        [Route("SendAlert")]
        public IActionResult SendAlert([FromBody]ChefRequest chefRequest)
        {
            foreach(var item in chefRequest.items)
            {
                var connectionIds = _connectionManager.GetConnections(item.chef);
                _hubContext.Clients.Client(connectionIds.FirstOrDefault()).InvokeAsync("OnAlertReceived", "Alert Received");
            }
            return Ok();
        }

        [HttpGet]
        [Route("TestPing")]
        public string TestPing()
        {
            return "Test";
        }
    }
}
