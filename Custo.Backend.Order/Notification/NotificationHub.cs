﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Custo.Backend.Order.Business;
using Custo.Backend.Order.Entities.APIRequest;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;

namespace Custo.Backend.Order.Notification
{
    public class NotificationHub : Hub
    {
        private readonly IConnectionManager _connectionManager;
        private readonly IOrderManager _orderManager;
        private readonly IOrderTracker _orderTracker;
        public NotificationHub(IOrderManager orderManager, IOrderTracker orderTracker, IConnectionManager connectionManager)
        {
            _orderManager = orderManager;
            _orderTracker = orderTracker;
            _connectionManager = connectionManager;
        }
        //[Authorize(Policy = "Chef")]
        public void RegisterClient(string clientId)
        {
            _connectionManager.Add(clientId, Context.ConnectionId);
        }
        //[Authorize(Policy = "Chef")]
        public void DeRegisterClient(string clientId)
        {
            _connectionManager.Remove(clientId, Context.ConnectionId);
        }

        //[Authorize(Policy = "Chef")]
        public override Task OnConnectedAsync()
        {
            return base.OnConnectedAsync();
        }
        //[Authorize(Policy = "Chef")]
        public override Task OnDisconnectedAsync(Exception exception)
        {
            return base.OnDisconnectedAsync(exception);
        }
        //[Authorize(Policy = "Handler")]
        public void OnOrderConfirmation(ChefRequest orderDetails)
        {
            _orderTracker.AddOrder(orderDetails);
            orderDetails.items.ToList().ForEach(x => Clients.Client(_connectionManager.GetConnections(x.chef).FirstOrDefault()).InvokeAsync("OnChefAssigning", JsonConvert.SerializeObject(new FoodRequest { item = x, order_id = orderDetails.order_id, order_time = DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalSeconds })));
        }
       
        //[Authorize(Policy = "Chef")]
        public void OnStatusUpdate(FoodRequest foodOrder)
        {
            var orderDetails = _orderTracker.UpdateOrder(foodOrder).Result;

            //if (orderDetails.status == "Onstove")
            //{
            //    Clients.Client(_connectionManager.GetConnections("handlerId").FirstOrDefault()).InvokeAsync("OnOrderAssigning", JsonConvert.SerializeObject(orderDetails));
            //}
            //if (orderDetails.status == "Completed")
            //{
            //    Clients.Client(_connectionManager.GetConnections("handlerId").FirstOrDefault()).InvokeAsync("OnOrderCompleted", JsonConvert.SerializeObject(orderDetails));
            //    _orderTracker.RemoveOrder(orderDetails.order_id);
            //}
        }

        public void OnOrderRejection()
        {

        }

        public void OnOrderFetch(string orderId)
        {
            var orderDetails = _orderTracker.GetOrder(orderId).Result;
            Clients.Client(_connectionManager.GetConnections("handlerId").FirstOrDefault()).InvokeAsync("OnGetOrder", orderDetails);
        }
    }
}