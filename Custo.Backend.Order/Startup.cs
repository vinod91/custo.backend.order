﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Custo.Backend.Order.Business;
using Custo.Backend.Order.DataAccess;
using Custo.Backend.Order.Notification;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Custo.Backend.Order
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            //        .AddJwtBearer(options =>
            //        {
            //            options.TokenValidationParameters = new TokenValidationParameters
            //            {
            //                ValidateIssuer = true,
            //                ValidateAudience = false,
            //                ValidateLifetime = true,
            //                ValidateIssuerSigningKey = true,
            //                ValidIssuer = "https://cognito-idp.us-east-1.amazonaws.com/us-east-1_Gb6TxRxNE",
            //                IssuerSigningKey = this.SigningKey("jATYr8X2NMs8TBIRI3m2XX5nDoWNPHBDYJRl99aZByq7DLs6uajjKHkV1-1JDdsROMUtpBEqvl_riQqwaslBHf9O5MrgqoUHd9BDETeGTf9I6QPNOypFDSdEoSwwetJSrmxOMk4xTylwzKarRuvX6SlFfpk1FXg3C9CwJfGzCE0Px5r62B5YC8phZ5br_iVCDQbMm6cDOnU-TiSIUe83a4-IjXClCg_ugJN7nxnRxyfcEcGLPn4mklKsDv2slMSpueL65Vm6zo2RUnXSv9XAu7AqcqMzqIJH_kG-orMCL6pCaunTRoN1hV5dCxBVI7kQIiJxZTnDySKH-JqyOgcdhw", "AQAB")
            //            };
            //        });
          
            //services.AddAuthorization(options =>
            //{
            //    options.AddPolicy("Customer", new AuthorizationPolicyBuilder()
            //           .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme‌​)
            //           .RequireAuthenticatedUser()
            //           .RequireClaim("custom:role", "customer")
            //           .Build());
            //});
            //services.AddAuthorization(options =>
            //{
            //    options.AddPolicy("Handler", new AuthorizationPolicyBuilder()
            //           .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme‌​)
            //           .RequireAuthenticatedUser()
            //           .RequireClaim("custom:role", "handler")
            //           .Build());
            //});
            //services.AddAuthorization(options =>
            //{
            //    options.AddPolicy("Chef", new AuthorizationPolicyBuilder()
            //           .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme‌​)
            //           .RequireAuthenticatedUser()
            //           .RequireClaim("custom:role", "chef")
            //           .Build());
            //});

            services.AddCors(options =>
            {
                options.AddPolicy("Orders",
                    policy => policy.AllowAnyOrigin()
                                    .AllowAnyHeader()
                                    .AllowAnyMethod());
            });

            services.AddSignalR();
            services.AddMvc();
            services.AddMemoryCache();
            services.AddDistributedRedisCache(options =>
            {
                options.Configuration = "127.0.0.1:6379";
                options.InstanceName = "order";
            });
            services.AddTransient<IOrderRepository, OrderRepository>();
            services.AddTransient<IOrderManager, OrderManager>();
            services.AddTransient<IOrderTracker, OrderTracker>();
            services.AddTransient<IConnectionManager, ConnectionManager>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseAuthentication();
            app.UseMvc();
            app.UseCors("Orders");
            app.UseSignalR(routes =>
            {
                routes.MapHub<NotificationHub>("notificationHub");
            });
        }

        public RsaSecurityKey SigningKey(string key, string expo)
        {
            using (RSA rsa = RSA.Create())
            {
                rsa.ImportParameters(
                new RSAParameters
                {
                    Modulus = Base64UrlEncoder.DecodeBytes(key),
                    Exponent = Base64UrlEncoder.DecodeBytes(expo)
                }
                );
                return new RsaSecurityKey(rsa);
            }
        }
    }
}
