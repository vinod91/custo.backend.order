﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Custo.Backend.Order.Entities.DB
{
    public class ChefDetails
    {
            public Dictionary<string, HashSet<int>>[] ChefId { get; set; }
            public int RestaurantId { get; set; }
    }
}
