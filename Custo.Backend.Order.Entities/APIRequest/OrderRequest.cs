﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Custo.Backend.Order.Entities.APIRequest
{
    public class ChefRequest
    {
        public IEnumerable<Item> items { get; set; }
        public string order_id { get; set; }
        public int restaurant_id { get; set; }
        public string status { get; set; }
    }

    public class FoodRequest
    {
        public Item item { get; set; }
        public string order_id { get; set; }
        public double order_time { get; set; }
    }

    public class Item
    {
        public string name { get; set; }
        public double price { get; set; }
        public int quantity { get; set; }
        public int id { get; set; }
        public string cuisine { get; set; }
        public string status { get; set; }
        public string chef { get; set; }
        public int preptime { get; set; }
        public string customization { get; set; }
    }

    public class Charge
    {
        public string name { get; set; }
        public double value { get; set; }
    }

    public class UserInfo
    {
        public string name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string street { get; set; }
        public string locality { get; set; }
        public string city { get; set; }
    }

    public class OrderRequest: ChefRequest
    {
        public IEnumerable<Charge> charges { get; set; }
        public UserInfo user_info { get; set; }
        public string type { get; set; }
        public string order_time { get; set; }
        public string payment_mode { get; set; }
        public string payment_status { get; set; }
        public int scheduled_time { get; set; }
    }
}

