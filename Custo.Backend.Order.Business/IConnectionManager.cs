﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Custo.Backend.Order.Business
{
    public interface IConnectionManager
    {
        void Add(string userId, string connectionId);
        IEnumerable<string> GetConnections(string userId);
        void Remove(string userId, string connectionId);
    }
}
