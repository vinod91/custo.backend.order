﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Custo.Backend.Order.Entities.DB;

namespace Custo.Backend.Order.Business
{
    public interface IOrderManager
    {
        Task<Dictionary<string, HashSet<int>>[]> FetchChefDetails(int restaurantId);
    }
}
