﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Custo.Backend.Order.Business;
using Microsoft.Extensions.Caching.Memory;

namespace Custo.Backend.Order.Notification
{
    public class ConnectionManager : IConnectionManager
    {
        private readonly IMemoryCache _connectionCache;

        public ConnectionManager(IMemoryCache memoryCache)
        {
            _connectionCache = memoryCache;
        }

        public void Add(string userId, string connectionId)
        {
            lock (_connectionCache)
            {
                HashSet<string> connections;
                if (!_connectionCache.TryGetValue(userId, out connections))
                {
                    connections = new HashSet<string>();
                    _connectionCache.Set(userId, connections, new MemoryCacheEntryOptions { Priority = CacheItemPriority.High });
                }

                lock (connections)
                {
                    connections.Add(connectionId);
                }
            }
        }

        public IEnumerable<string> GetConnections(string userId)
        {
            HashSet<string> connections;
            if (_connectionCache.TryGetValue(userId, out connections))
            {
                return connections;
            }

            return Enumerable.Empty<string>();
        }

        public void Remove(string userId, string connectionId)
        {
            lock (_connectionCache)
            {
                HashSet<string> connections;
                if (!_connectionCache.TryGetValue(userId, out connections))
                {
                    return;
                }

                lock (connections)
                {
                    connections.Remove(connectionId);

                    if (connections.Count == 0)
                    {
                        _connectionCache.Remove(userId);
                    }
                }
            }
        }
    }
}

