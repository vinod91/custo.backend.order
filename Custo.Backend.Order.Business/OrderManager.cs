﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Custo.Backend.Order.DataAccess;
using Custo.Backend.Order.Entities.DB;

namespace Custo.Backend.Order.Business
{
    public class OrderManager: IOrderManager
    {
        private readonly IOrderRepository _orderRepository;
        public OrderManager(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }
        public Task<Dictionary<string, HashSet<int>>[]> FetchChefDetails(int restaurantId)
        {
            return  _orderRepository.FetchChefDetails(restaurantId);
        }
    }
}
