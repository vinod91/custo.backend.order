﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Custo.Backend.Order.Entities.APIRequest;

namespace Custo.Backend.Order.Business
{
    public interface IOrderTracker
    {
        Task AddOrder(ChefRequest orderDetails);
        Task RemoveOrder(string orderId);
        Task<ChefRequest> UpdateOrder(FoodRequest foodRequest);
        Task<string> GetOrder(string orderid);
    }
}
