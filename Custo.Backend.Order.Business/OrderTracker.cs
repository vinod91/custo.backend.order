﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Custo.Backend.Order.Entities.APIRequest;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace Custo.Backend.Order.Business
{
    public class OrderTracker : IOrderTracker
    {
        private readonly IDistributedCache _orderCache;

        public OrderTracker(IMemoryCache orderDetailsCache, IDistributedCache orderCache)
        {
            _orderCache = orderCache;
        }

        public async Task AddOrder(ChefRequest orderDetails)
        {
            await _orderCache.SetStringAsync(orderDetails.order_id, JsonConvert.SerializeObject(orderDetails));
        }

        public async Task RemoveOrder(string orderId)
        {
           await _orderCache.RemoveAsync(orderId);
        }

        public async Task<ChefRequest> UpdateOrder(FoodRequest foodRequest)
        {
            var orderDetails = await _orderCache.GetStringAsync(foodRequest.order_id);
            if (!string.IsNullOrEmpty(orderDetails))
            {
                var orderFood = JsonConvert.DeserializeObject<ChefRequest>(orderDetails);
                for(int i =0;i < orderFood.items.Count();i++)
                {
                    if (orderFood.items.ToArray()[i].id == foodRequest.item.id)
                        orderFood.items.ToArray()[i].status = foodRequest.item.status;
                }
                
                //orderFood.items.ToList().ForEach(x => x = x.id == foodRequest.item.id ? foodRequest.item : x);
                orderFood.status = orderFood.items.ToList().TrueForAll(x => x.status == "Completed" || x.status == "Onstove") ? "Onstove" : orderFood.status;
                orderFood.status = orderFood.items.ToList().TrueForAll(x => x.status == "Completed") ? "Completed" : orderFood.status;
                await _orderCache.SetStringAsync(foodRequest.order_id, JsonConvert.SerializeObject(orderFood));
                return orderFood;
            }
            return null;
        }

        public async Task<string> GetOrder(string orderid)
        {
            return await _orderCache.GetStringAsync(orderid);
        }
    }
}
